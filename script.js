console.log ('JS ES6 Updates')

// 1. Exponent Operator

const firstNum = 8 ** 2; // before 
console.log (firstNum);

let secondNum =  Math.pow (8,2); //update
console.log (secondNum);

// 2. Template Literals (``)
//allows to write strings without using concatenation operator (+)
// helps with code readability 


let name = 'Noctis';

let message = 'Hello ' + name + '! Welcome to programming!' ;

console.log ('message without template literals:' +
message);  // before 


//UPDATED 

message = `Hello ${name} ! Welcome to programming!` ;
console.log (`template literals ${message} `) ; 

let anotherMessage = ` ${name} attended a Math competition. 
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}`

console.log(anotherMessage);

let interestRate = .1; 
let principal = 1000;
console.log (`The interest on your savings account is ${principal * interestRate}`);

//3. Array Destructuring 

// - allows to unpack elements in arrays into distinct variable 
//- allows us to name array elements with variable instead of using index numbers 

let fullName = ['Noctis' , 'Lucis', 'Caelum'];

//before

console.log (`Hello ${fullName [0]} ${fullName [1]} ${fullName [2]}! Hajimemashite! `);


//UPDATED 

let [firstName, middleName, lastName] = fullName;

console.log (firstName);
console.log (middleName);
console.log (lastName);
console.log (`Hello ${firstName} ${middleName} ${lastName}! Hajimemashite! `);


//4. Object Destructuring 
//allos to unpack properties of an object into distinct variables

let person = {
    givenName: 'Lunafreya',
    maidenName: 'Nox', 
    familyName: 'Flueret', 

}; 

//before 

console.log (person.givenName);
console.log (person.maidenName);
console.log (person.familyName);
console.log (`Hello ${person.givenName} ${person.maidenName} ${person.familyName}!
Welcome to Lucis! `);


//updated 

let {givenName, maidenName, familyName } = person;

console.log (givenName);
console.log (maidenName);
console.log (familyName);
console.log (`Hello ${givenName} ${maidenName} ${familyName}!
Welcome to Lucis! `);



let person2 = {
    gName: 'Cloud', 
    lName: 'Strife',
    attack: function() {
        return `${gName} used Curaga! `
    }
};

let {gName, lName, attack} = person2;

console.log (`oy baka ${gName} ${lName}`);
console.log (attack);
console.log (attack());

//5. ARROW FUNCTION 

/*- compact alternative syntax to traditional functions
- useful for code snippets where creating functions will not be reused in any other portion of the code
- syntax:
const/let variableName = (parameters) => {statements};
*/



//before
const hello = function(){
	console.log("Hello world!");
}

//using arrow function
const helloAgain = () => {
	console.log("Hello world!");
}

hello();
helloAgain();

let goodNight = () => {
    console.log ('Send help please');
}
goodNight();



// ARROW FUNCTION WITH LOOPS

let soldier =['Cloud' , 'Zack', 'Angeal'];

soldier.forEach((soldier) => {
    console.log (`${soldier} is filled with Mako Energy.`)
});

let numbers = [1, 2, 3, 4, 5];

//before
let numberMap = numbers.map(function(number){
	return number * number;
});
console.log(numberMap);

//using arrow function
let numberMap2 = numbers.map((number) => {
	return number * number;
});
console.log(numberMap2);



// 6. IMPLICIT RETURN STATEMENT 
// there are some instances when you can omit the "return statement"
// JS implicitly adds it for the result of the function

// const add = (x, y) => { return x + y; };
const add = (x, y) => x + y;
let total = add(1, 2);
console.log(total);


// 7. Default Function Argument Value
// provides a default argument value if none is provided 

let greet = (name = 'Netizen') => {
    return `What are you doing, ${name}?! ` ;
}
console.log (greet ());
console.log (greet ('Rie'));

// 8. class based object blueprint 

//- allows creation/instantiation of objects using classes as blueprints

//Creating a class
//- the constructor is a special method of a class for creating an object for that class

class materia {
    constructor (magic, range, user) {
        this.magic = magic;
        this.range = range;  
        this.user = user;  
    }
}

let myMateria = new materia ();
console.log (myMateria);

myMateria.magic = "Cura";
myMateria.range = "Any Range";
myMateria.user = "Aerith";
console.log (myMateria);


let myNewMateria = new materia ("Thundaga","Far Range", "Cloud" );
console.log (myNewMateria);

let myNewMateria2 = new materia ("Esuna","Any Range", "Tifa" );
console.log (myNewMateria2);

